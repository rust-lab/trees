mod arena_tree;

pub use crate::arena_tree::{Node, NodeArena};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
