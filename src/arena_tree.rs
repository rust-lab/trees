use std::cmp::PartialEq;
use std::fmt::Debug;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
pub struct NodeArena<T: Debug + PartialEq> {
    nodes: Vec<Option<Node<T>>>,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NodeIndex {
    pub index: usize,
}

impl NodeIndex {
    pub fn new(index: usize) -> NodeIndex {
        NodeIndex{index,}
    }
}

impl Display for NodeIndex {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "NodeIndex[{}]", self.index)
    }
}

#[derive(Debug)]
pub struct Node<T: Debug + PartialEq> {
    pub index: NodeIndex,
    pub parent: Option<NodeIndex>,
    children: Vec<NodeIndex>,
    data: Box<T>,
}

impl<T: Debug + PartialEq> NodeArena<T> {
    pub fn new() -> NodeArena<T> {
        NodeArena::<T> { nodes: Vec::new() }
    }

    pub fn new_node(&mut self, data: T) -> NodeIndex {
        let i = NodeIndex {
            index: self.nodes.len(),
        };

        self.nodes.push(Some(Node::<T> {
            index: i,
            parent: Option::None,
            children: Vec::new(),
            data: Box::new(data),
        }));

        i
    }

    pub fn nodes(&self) -> &Vec<Option<Node<T>>> {
        &self.nodes
    }

    pub fn get_parent(&self, idx: NodeIndex) -> Option<NodeIndex> {
        match self.get(idx) {
            None => None,
            Some(n) => n.parent
        }
    }

    pub fn add_child(&mut self, father_idx: NodeIndex, data: T) -> NodeIndex {
        let i = NodeIndex {
            index: self.nodes.len(),
        };

        self.nodes.push(Some(Node::<T> {
            index: i,
            parent: Option::Some(father_idx),
            children: Vec::new(),
            data: Box::new(data),
        }));

        let father = self.get_mut(father_idx).expect(&format!("Nothing at {}", father_idx));
        father.children.push(i);

        i
    }

    pub fn link(&mut self, father: NodeIndex, child: NodeIndex) {
        {
            let c = self.get_mut(child).expect(&format!("Nothing at {}", child.index));
            assert!(c.parent.is_none());
            c.parent = Some(father);
        }

        {
            let f = self.get_mut(father).expect(&format!("Nothing at {}", father.index));
            f.children.push(child);
        }
    }

    fn get_mut(&mut self, idx: NodeIndex) -> Option<&mut Node<T>> {
        match self.nodes
            .as_mut_slice()
            .get_mut(idx.index)
            {
                None => None,
                Some(opt_node) => match opt_node {
                    None => None,
                    some => some.as_mut()
                }
            }
            
    }

    pub fn get(&self, idx: NodeIndex) -> Option<&Node<T>> {
        let opt_node = self.nodes
            .get(idx.index).expect(&format!("Nothing at {}", idx.index));
        match opt_node {
            None => None,
            Some(node) => Some(&node)
        }
    }

    pub fn find_mut_data(&mut self, searched: T) -> Option<NodeIndex> {
        for opt_node in &self.nodes {
            if let Some(node) = opt_node {
                if node.data.as_ref() == &searched {
                    return Some(node.index);
                }
            }
        }
        Option::None
    }

    pub fn depth(&self, node_idx: NodeIndex) -> u128 {
        let n = self.get(node_idx).expect(&format!("Nothing at {}", node_idx.index));
        match n.parent {
            None => 0,
            Some(p) => 1 + self.depth(p),
        }
    }

    pub fn get_root(&self) -> NodeIndex {
        assert!(!self.nodes.is_empty());

        self.internal_get_root(NodeIndex { index: 0 })
    }

    fn internal_get_root(&self, idx: NodeIndex) -> NodeIndex {
        match self.get(idx).expect(&format!("Nothing at {}", idx.index)).parent {
            None => idx,
            Some(p) => self.internal_get_root(p),
        }
    }

    pub fn is_ancestor(&self, start: NodeIndex, ancestor: NodeIndex) -> bool {
        if start == ancestor {
            true
        } else {
            let n = self.get(start).expect(&format!("Nothing at {}", start.index));
            match n.parent {
                None => false,
                Some(i) => self.is_ancestor(i, ancestor),
            }
        }
    }

    pub fn distance(&self, child: NodeIndex, ancestor: NodeIndex, verify: bool) -> u128 {
        if child == ancestor {
            0
        } else {
            if verify {
                if !self.is_ancestor(child, ancestor) {
                    return self.distance(ancestor, child, false);
                }
            }

            let p = self.get(child).expect(&format!("Nothing at {}", child.index)).parent.expect("No parent");

            1 + self.distance(p, ancestor, false)
        }
    }
}

impl<T: Debug + PartialEq> Node<T> {}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_global() {
        let mut arena = NodeArena::<i32>::new();
        let root = arena.new_node(1);
        let child1 = arena.add_child(root, 2);
        let child2 = arena.add_child(root, 3);
        let child3 = arena.add_child(child1, 4);
        println!("{:?}", arena);
        println!("{:?}", root);

        assert_eq!(root, arena.get(child1).expect(&format!("Nothing at {}", child1)).parent.expect("oops"));
        assert!(arena.get(root).expect(&format!("Nothing at {}", root)).children.contains(&child1));
        assert_eq!(0, arena.depth(root));
        assert_eq!(1, arena.depth(child1));
        assert_eq!(2, arena.depth(child3));

        assert_eq!(child3, arena.find_mut_data(4).expect("oopsie"));

        assert_eq!(root, arena.internal_get_root(child3));

        assert!(arena.is_ancestor(child3, root));
        assert!(!arena.is_ancestor(child3, child2));
        assert!(arena.is_ancestor(child3, child3));
        assert!(!arena.is_ancestor(root, child2));
    }

}
